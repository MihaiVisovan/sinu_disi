USE [BDSinu]
GO
SET IDENTITY_INSERT [dbo].[Facultate] ON 

INSERT [dbo].[Facultate] ([idFacultate], [denumire], [specializare]) VALUES (1, N'Calculatoare', N'Tehnologia Informatiei')
INSERT [dbo].[Facultate] ([idFacultate], [denumire], [specializare]) VALUES (2, N'Calculatoare', N'Calculatoare')
INSERT [dbo].[Facultate] ([idFacultate], [denumire], [specializare]) VALUES (3, N'Automatica', N'Automatica')
SET IDENTITY_INSERT [dbo].[Facultate] OFF
SET IDENTITY_INSERT [dbo].[Materie] ON 

INSERT [dbo].[Materie] ([idMaterie], [denumire], [idProfesor], [numarDeCredite]) VALUES (1, N'Analiza Matematica', 1, 6)
INSERT [dbo].[Materie] ([idMaterie], [denumire], [idProfesor], [numarDeCredite]) VALUES (2, N'Programare Funtionala', 1, 10)
INSERT [dbo].[Materie] ([idMaterie], [denumire], [idProfesor], [numarDeCredite]) VALUES (3, N'Proiectarea cu microprocesoare', 8, 8)
INSERT [dbo].[Materie] ([idMaterie], [denumire], [idProfesor], [numarDeCredite]) VALUES (4, N'Arhitectura Calculatoarelor', 8, 8)
INSERT [dbo].[Materie] ([idMaterie], [denumire], [idProfesor], [numarDeCredite]) VALUES (5, N'Proiectare Software', 9, 7)
INSERT [dbo].[Materie] ([idMaterie], [denumire], [idProfesor], [numarDeCredite]) VALUES (6, N'Sisteme distribuite', 9, 5)
SET IDENTITY_INSERT [dbo].[Materie] OFF
SET IDENTITY_INSERT [dbo].[SpecializareMaterie] ON 

INSERT [dbo].[SpecializareMaterie] ([idSpecializareMaterie], [idFacultate], [idMaterie]) VALUES (1, 1, 1)
INSERT [dbo].[SpecializareMaterie] ([idSpecializareMaterie], [idFacultate], [idMaterie]) VALUES (2, 2, 2)
INSERT [dbo].[SpecializareMaterie] ([idSpecializareMaterie], [idFacultate], [idMaterie]) VALUES (3, 3, 1)
SET IDENTITY_INSERT [dbo].[SpecializareMaterie] OFF
SET IDENTITY_INSERT [dbo].[Grupa] ON 

INSERT [dbo].[Grupa] ([idGrupa], [numarGrupa], [an], [nrMaxStudentiPerGrupa]) VALUES (1, 3011, N'1', 10)
INSERT [dbo].[Grupa] ([idGrupa], [numarGrupa], [an], [nrMaxStudentiPerGrupa]) VALUES (2, 3012, N'1', 10)
INSERT [dbo].[Grupa] ([idGrupa], [numarGrupa], [an], [nrMaxStudentiPerGrupa]) VALUES (3, 3021, N'2', 10)
INSERT [dbo].[Grupa] ([idGrupa], [numarGrupa], [an], [nrMaxStudentiPerGrupa]) VALUES (4, 3022, N'2', 10)
INSERT [dbo].[Grupa] ([idGrupa], [numarGrupa], [an], [nrMaxStudentiPerGrupa]) VALUES (5, 3031, N'3', 10)
INSERT [dbo].[Grupa] ([idGrupa], [numarGrupa], [an], [nrMaxStudentiPerGrupa]) VALUES (6, 3032, N'3', 10)
INSERT [dbo].[Grupa] ([idGrupa], [numarGrupa], [an], [nrMaxStudentiPerGrupa]) VALUES (1005, 3015, N'3', 50)
SET IDENTITY_INSERT [dbo].[Grupa] OFF
SET IDENTITY_INSERT [dbo].[User] ON 

INSERT [dbo].[User] ([idUser], [username], [password], [nume], [prenume], [telefon], [CNP], [rol], [email], [imagine]) VALUES (1, N'popion', N'popica', N'Pop', N'Ion', N'0734128945', N'1231069432345', N'PROFESOR', N'po_ion@gmai.com', NULL)
INSERT [dbo].[User] ([idUser], [username], [password], [nume], [prenume], [telefon], [CNP], [rol], [email], [imagine]) VALUES (2, N'rodica10', N'secret', N'Ignatescu', N'Rodica', N'0735894324', N'2020202114523', N'SECRETAR', N'roditutza_02@yahoo.ro', NULL)
INSERT [dbo].[User] ([idUser], [username], [password], [nume], [prenume], [telefon], [CNP], [rol], [email], [imagine]) VALUES (3, N'Laryon', N'yon', N'Larion', N'Mirel', N'0745156900', N'1121295335599', N'STUDENT', N'laryion_mirel@gmail.com', NULL)
INSERT [dbo].[User] ([idUser], [username], [password], [nume], [prenume], [telefon], [CNP], [rol], [email], [imagine]) VALUES (4, N'usergreu', N'parolausoara', N'Mere', N'Anaf', N'0745981279', N'2071197995679', N'STUDENT', N'ana.mere@yahoo.com', NULL)
INSERT [dbo].[User] ([idUser], [username], [password], [nume], [prenume], [telefon], [CNP], [rol], [email], [imagine]) VALUES (5, N'namidee', N'niciaici', N'Zinhong', N'Chan', N'0715945672', N'1041094887142', N'STUDENT', N'zin.chan@yahoo.com', NULL)
INSERT [dbo].[User] ([idUser], [username], [password], [nume], [prenume], [telefon], [CNP], [rol], [email], [imagine]) VALUES (6, N'whoami', N'iammyself', N'Ionescu', N'Dorel', N'0751094235', N'1010598128942', N'STUDENT', N'io_dorel@gmail.com', NULL)
INSERT [dbo].[User] ([idUser], [username], [password], [nume], [prenume], [telefon], [CNP], [rol], [email], [imagine]) VALUES (7, N'zaname', N'zapassword', N'Doinita', N'Doina', N'0731984234', N'2010994224415', N'STUDENT', N'doina.x2@yahoo.ro', NULL)
INSERT [dbo].[User] ([idUser], [username], [password], [nume], [prenume], [telefon], [CNP], [rol], [email], [imagine]) VALUES (8, N'lenuta', N'l', N'Taran', N'Lenuta', N'0735782398', N'2010969350942', N'PROFESOR', N'lenuta_taran@gmail.com', NULL)
INSERT [dbo].[User] ([idUser], [username], [password], [nume], [prenume], [telefon], [CNP], [rol], [email], [imagine]) VALUES (9, N'mirciuleanu', N'leprofesor', N'Mirciuleanu', N'Mircea', N'0743561298', N'1030556231114', N'PROFESOR', N'mirciuleanu_mircea@gmail.com', NULL)
INSERT [dbo].[User] ([idUser], [username], [password], [nume], [prenume], [telefon], [CNP], [rol], [email], [imagine]) VALUES (10, N'zenovia9', N'zenoviaveche', N'Sminureanu', N'Zenovia', N'0731441244', N'2011185518302', N'STUDENT', N'zeny4u@yahoo.ro', NULL)
INSERT [dbo].[User] ([idUser], [username], [password], [nume], [prenume], [telefon], [CNP], [rol], [email], [imagine]) VALUES (14, N'muthiIonut', N'muthi', N'Muthi', N'Ionut', N'0234243', N'0329403240', N'STUDENT', N'muthi@yahoo', NULL)
INSERT [dbo].[User] ([idUser], [username], [password], [nume], [prenume], [telefon], [CNP], [rol], [email], [imagine]) VALUES (1012, N'sara_popa', N'sara', N'Sara', N'Popa', N'0744235124', N'192911919191', N'STUDENT', N'sara_c_ppopa@yahoo.com', NULL)
INSERT [dbo].[User] ([idUser], [username], [password], [nume], [prenume], [telefon], [CNP], [rol], [email], [imagine]) VALUES (1013, N'danarosca', N'dana', N'Dana', N'Rosca', N'0742319203', N'1961322321', N'STUDENT', N'dana_rosca@yahoo.com', NULL)
INSERT [dbo].[User] ([idUser], [username], [password], [nume], [prenume], [telefon], [CNP], [rol], [email], [imagine]) VALUES (1014, N'test', N'123', N'dana', N'backed', N'1233', N'123', N'STUDENT', N'dsa@yahoo', NULL)
SET IDENTITY_INSERT [dbo].[User] OFF
SET IDENTITY_INSERT [dbo].[StudentGrupa] ON 

INSERT [dbo].[StudentGrupa] ([idStudentGrupa], [idStudent], [idGrupa]) VALUES (1, 3, 1)
INSERT [dbo].[StudentGrupa] ([idStudentGrupa], [idStudent], [idGrupa]) VALUES (2, 4, 2)
INSERT [dbo].[StudentGrupa] ([idStudentGrupa], [idStudent], [idGrupa]) VALUES (3, 5, 3)
INSERT [dbo].[StudentGrupa] ([idStudentGrupa], [idStudent], [idGrupa]) VALUES (4, 6, 4)
INSERT [dbo].[StudentGrupa] ([idStudentGrupa], [idStudent], [idGrupa]) VALUES (5, 7, 5)
INSERT [dbo].[StudentGrupa] ([idStudentGrupa], [idStudent], [idGrupa]) VALUES (6, 10, 6)
INSERT [dbo].[StudentGrupa] ([idStudentGrupa], [idStudent], [idGrupa]) VALUES (1010, 14, 1)
INSERT [dbo].[StudentGrupa] ([idStudentGrupa], [idStudent], [idGrupa]) VALUES (1011, 1014, 1)
SET IDENTITY_INSERT [dbo].[StudentGrupa] OFF
SET IDENTITY_INSERT [dbo].[Nota] ON 

INSERT [dbo].[Nota] ([idNota], [idMaterie], [idStudent], [nota]) VALUES (1, 1, 3, 10)
INSERT [dbo].[Nota] ([idNota], [idMaterie], [idStudent], [nota]) VALUES (2, 2, 4, 10)
INSERT [dbo].[Nota] ([idNota], [idMaterie], [idStudent], [nota]) VALUES (3, 1, 4, 9)
INSERT [dbo].[Nota] ([idNota], [idMaterie], [idStudent], [nota]) VALUES (4, 2, 3, 5)
INSERT [dbo].[Nota] ([idNota], [idMaterie], [idStudent], [nota]) VALUES (5, 3, 5, 8)
INSERT [dbo].[Nota] ([idNota], [idMaterie], [idStudent], [nota]) VALUES (6, 3, 6, 5)
INSERT [dbo].[Nota] ([idNota], [idMaterie], [idStudent], [nota]) VALUES (12, 4, 7, 8)
INSERT [dbo].[Nota] ([idNota], [idMaterie], [idStudent], [nota]) VALUES (13, 5, 10, 5)
INSERT [dbo].[Nota] ([idNota], [idMaterie], [idStudent], [nota]) VALUES (14, 6, 3, 2)
INSERT [dbo].[Nota] ([idNota], [idMaterie], [idStudent], [nota]) VALUES (15, 5, 6, 8)
INSERT [dbo].[Nota] ([idNota], [idMaterie], [idStudent], [nota]) VALUES (16, 6, 7, 5)
SET IDENTITY_INSERT [dbo].[Nota] OFF
SET IDENTITY_INSERT [dbo].[Sesiune] ON 

INSERT [dbo].[Sesiune] ([idSesiune], [idMaterie], [dataSiOra], [sala]) VALUES (1, 1, CAST(N'2019-09-12T14:00:00.000' AS DateTime), N'D5')
INSERT [dbo].[Sesiune] ([idSesiune], [idMaterie], [dataSiOra], [sala]) VALUES (1003, 3, CAST(N'2019-05-08T18:59:26.637' AS DateTime), N'41')
INSERT [dbo].[Sesiune] ([idSesiune], [idMaterie], [dataSiOra], [sala]) VALUES (1004, 3, CAST(N'2019-05-08T20:45:36.360' AS DateTime), N'41')
SET IDENTITY_INSERT [dbo].[Sesiune] OFF
SET IDENTITY_INSERT [dbo].[StudentMaterie] ON 

INSERT [dbo].[StudentMaterie] ([idStudentMaterie], [idStudent], [idMaterie]) VALUES (1, 3, 1)
INSERT [dbo].[StudentMaterie] ([idStudentMaterie], [idStudent], [idMaterie]) VALUES (2, 3, 2)
INSERT [dbo].[StudentMaterie] ([idStudentMaterie], [idStudent], [idMaterie]) VALUES (3, 4, 1)
INSERT [dbo].[StudentMaterie] ([idStudentMaterie], [idStudent], [idMaterie]) VALUES (4, 4, 2)
INSERT [dbo].[StudentMaterie] ([idStudentMaterie], [idStudent], [idMaterie]) VALUES (5, 5, 3)
INSERT [dbo].[StudentMaterie] ([idStudentMaterie], [idStudent], [idMaterie]) VALUES (6, 5, 4)
INSERT [dbo].[StudentMaterie] ([idStudentMaterie], [idStudent], [idMaterie]) VALUES (7, 5, 6)
INSERT [dbo].[StudentMaterie] ([idStudentMaterie], [idStudent], [idMaterie]) VALUES (8, 6, 5)
INSERT [dbo].[StudentMaterie] ([idStudentMaterie], [idStudent], [idMaterie]) VALUES (9, 6, 1)
INSERT [dbo].[StudentMaterie] ([idStudentMaterie], [idStudent], [idMaterie]) VALUES (10, 7, 6)
INSERT [dbo].[StudentMaterie] ([idStudentMaterie], [idStudent], [idMaterie]) VALUES (11, 7, 2)
INSERT [dbo].[StudentMaterie] ([idStudentMaterie], [idStudent], [idMaterie]) VALUES (12, 7, 3)
INSERT [dbo].[StudentMaterie] ([idStudentMaterie], [idStudent], [idMaterie]) VALUES (13, 7, 6)
INSERT [dbo].[StudentMaterie] ([idStudentMaterie], [idStudent], [idMaterie]) VALUES (14, 10, 5)
SET IDENTITY_INSERT [dbo].[StudentMaterie] OFF
SET IDENTITY_INSERT [dbo].[Sala] ON 

INSERT [dbo].[Sala] ([idSala], [denumire]) VALUES (1, N'40')
INSERT [dbo].[Sala] ([idSala], [denumire]) VALUES (2, N'41')
INSERT [dbo].[Sala] ([idSala], [denumire]) VALUES (3, N'43p')
SET IDENTITY_INSERT [dbo].[Sala] OFF
