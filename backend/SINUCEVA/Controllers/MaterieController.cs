﻿using Sinu.Repository;
using SINUCEVA.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace SINUCEVA.Controllers
{
    public class MaterieController : ApiController
    {
        MaterieRepo m = new MaterieRepo();
        [HttpGet]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        [Route("api/getMaterieByIdProfesor")]
        public IHttpActionResult getMaterieByIdProfesor(int id)
        {
            return Ok(m.GetMaterie(id));
        }

        [HttpGet]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        [Route("api/getIdMaterieByDenumire")]
        public IHttpActionResult getIdMaterieByDenumire(string denumire)
        {
            return Ok(m.GetIdMaterie(denumire));
        }

        [HttpGet]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        [Route("api/getAllMaterie")]
        public IHttpActionResult getAllMaterie()
        {
            return Ok(m.getAllMaterie());
        }
    }
}
