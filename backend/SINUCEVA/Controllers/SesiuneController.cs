﻿using Sinu.Models;
using Sinu.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Sinu.Controllers
{
    public class SesiuneController : ApiController
    {
        SesiuneRepo s = new SesiuneRepo();
        [HttpGet]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        [Route("api/getSesiune")]
        public IHttpActionResult getSesiune()
        {
            return Ok(s.GetSesiuneDenumire());
        }

        [HttpPost]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        [Route("api/insertSesiune")]
        public IHttpActionResult insertSesiune([FromBody] Sesiune sesiune)
        {
            return Ok(s.InsertSesiune(sesiune));
        }

        [HttpPut]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        [Route("api/deleteSesiune")]
        public IHttpActionResult deleteSesiune(string id)
        {
            return Ok(s.DeleteSesiune(id));
        }

        [HttpPut]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        [Route("api/updateSesiune")]
        public IHttpActionResult updateSesiune([FromBody] Sesiune sesiune)
        {
            return Ok(s.updateSesiune(sesiune.IdSesiune.ToString(), sesiune.Sala));
        }
    }
}
