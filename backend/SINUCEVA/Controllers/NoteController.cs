﻿using Sinu.Models;
using Sinu.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Sinu.Controllers
{
    public class NoteController : ApiController
    {
        NoteRepo n = new NoteRepo();
        [HttpGet]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        [Route("api/getNote")]
        public IHttpActionResult getNoteByIdMaterie(string materie)
        {
            return Ok(n.GetNote(materie));
        }

        [HttpPost]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        [Route("api/insertNota")]
        public IHttpActionResult InsertNota([FromBody]Note nota)
        {
            return Ok(n.PutNote(nota));
        }

        [HttpPut]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        [Route("api/updateNota")]
        public IHttpActionResult UpdateNota([FromBody]Note nota)
        {
            return Ok(n.UpdateNote(nota));
        }
        [HttpGet]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        [Route("api/getNoteStudent")]
        public IHttpActionResult getNoteStudent(string id)
        {
            return Ok(n.getNoteStudent(id));
        }

        [HttpPut]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        [Route("api/notifiedStudent")]
        public IHttpActionResult NotifiedStudent([FromBody]Note nota)
        {
            return Ok(n.NotifiedStudent(nota));
        }
    }
}
