﻿using Sinu.Models;
using Sinu.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Sinu.Controllers
{
    public class LoginController : ApiController
    {
        LoginRepo log = new LoginRepo();
        [HttpGet]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        [Route("api/getRol")]
        public IHttpActionResult getRolByLogin(string username, string password)
        {
            return Ok(log.getRol(username, password));
        }
    }
}
