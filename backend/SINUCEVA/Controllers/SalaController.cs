﻿using SINUCEVA.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace SINUCEVA.Controllers
{
    public class SalaController : ApiController
    {

        SalaRepo sala = new SalaRepo();

        [HttpGet]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        [Route("api/getAllSala")]
        public IHttpActionResult getAllGroups()
        {
            var var = sala.getAllSala();
            return Ok(var);

        }
    }
}
