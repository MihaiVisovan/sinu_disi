﻿using Sinu.Models;
using Sinu.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Sinu.Controllers
{
    public class GrupaController : ApiController
    {
        GrupaRepo grp = new GrupaRepo();

        [HttpPost]
        [EnableCors(origins:"*",headers:"*",methods:"*")]
        [Route("api/addGrupaBySecretar")]
        public IHttpActionResult addGrupa([FromBody]Grupa grupa)
        {
            var test = grp.AddGrupaBySecretar(grupa);
            return Ok(test);
        }

        [HttpPost]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        [Route("api/addStudentToGrupa")]
        public IHttpActionResult addStudentToGrupa(int idStudent,int idGrupa)
        {
            var var = grp.AddStudentInGrupa(idStudent, idGrupa);
            return Ok(var);
        }

        [HttpDelete]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        [Route("api/deleteStudentFromGrupa")]
        public IHttpActionResult deleteStudentFromGrupa(int idStudent)
        {
            var var = grp.DeleteStudentFromGrupa(idStudent);
            return Ok(var);
        }

        [HttpPost]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        [Route("api/updateGrupaStudent")]
        public IHttpActionResult updateGrupaStudent(int idStudent,int idGrupa)
        {
            var var = grp.UpdateGrupaStudent(idGrupa, idStudent);
            return Ok(var);
        }

        [HttpGet]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        [Route("api/getAllGroups")]
        public IHttpActionResult getAllGroups()
        {
            var var = grp.GetAllGrupa();
            return Ok(var);
            
        }

        [HttpGet]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        [Route("api/getStudentNoGrupa")]
        public IHttpActionResult getStudentNoGrupa()
        {
            var var = grp.GetAllStudentNoGrupa();
            return Ok(var);

        }

        [HttpGet]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        [Route("api/getIdGrupaByName")]
        public IHttpActionResult getGrupaByName(int numarGrupa)
        {
            var var = grp.getIdGrupaByName(numarGrupa);
            return Ok(var);

        }

    }
}
