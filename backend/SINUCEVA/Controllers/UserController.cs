﻿using Sinu.Models;
using Sinu.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Sinu.Controllers
{
    public class UserController : ApiController
    {
        UserRepo usr = new UserRepo();
        [HttpPut]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        [Route("api/updateStudentBySecretar")]
        public IHttpActionResult updatePerson([FromBody]User user)
        {
            var test = usr.UpdatePersonBySecretar(user);
            return Ok(test);
        }

        [HttpGet]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        [Route("api/getAllUsers")]
        public IHttpActionResult getAllUsers()
        {
            var var = usr.GetAllUsers();
            return Ok(var);

        }

        [HttpGet]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        [Route("api/getStudentById")]
        public IHttpActionResult findStudentById(string id)
        {
            var test = usr.GetStudentById(id);
            return Ok(test);
        }

        [HttpGet]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        [Route("api/getStudentByEmail")]
        public IHttpActionResult findStudentByEmail(string email)
        {
            var test = usr.GetStudentByEmail(email);
            return Ok(test);
        }

        [HttpPost]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        [Route("api/insertUser")]
        public IHttpActionResult insertUser([FromBody]User user)
        {
            var test = usr.InsertUser(user);
            return Ok(test);
        }
        [HttpPut]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        [Route("api/updateStudent")]
        public IHttpActionResult updateStudent([FromBody]User user)
        {   
            var test = usr.UpdatePersonByStudent(user.Email, user.Telefon, user.IdUser.ToString());
            return Ok(test);
        }

    }
}
