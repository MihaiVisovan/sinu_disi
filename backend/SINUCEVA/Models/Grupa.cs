﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sinu.Models
{
    public class Grupa
    {
        public int idGrupa { get; set; }
        public string numarGrupa { get; set; }
        public string an { get; set; }
        public string nrMaxStudentiPerGrupa { get; set; }
    }
}