﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sinu.Models
{
    public class Materie
    {
        private int idMaterie;
        private int idProfesor;
        private string denumire;
        private string numarDeCredite;

        public int IdMaterie { get => idMaterie; set => idMaterie = value; }
        public int IdProfesor { get => idProfesor; set => idProfesor = value; }
        public string Denumire { get => denumire; set => denumire = value; }
        public string NumarDeCredite { get => numarDeCredite; set => numarDeCredite = value; }
    }
}