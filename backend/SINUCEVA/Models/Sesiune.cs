﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sinu.Models
{
    public class Sesiune
    {
        private int idSesiune;
        private int idMaterie;
        private DateTime dataOra;
        private string sala;
        private string denumire;

        public int IdSesiune { get => idSesiune; set => idSesiune = value; }
        public int IdMaterie { get => idMaterie; set => idMaterie = value; }
        public DateTime DataOra { get => dataOra; set => dataOra = value; }
        public string Sala { get => sala; set => sala = value; }
        public string Denumire { get => denumire; set => denumire = value; }
    }
}