﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sinu.Models
{
	public class NotaStudent
	{
        private int idUser;
        private int idNota;
        private int nota;
        private string nume;
        private string prenume;

        public int IdUser { get => idUser; set => idUser = value; }
        public int IdNota { get => idNota; set => idNota = value; }
        public int Nota { get => nota; set => nota = value; }
        public string Nume { get => nume; set => nume = value; }
        public string Prenume { get => prenume; set => prenume = value; }
    }
}