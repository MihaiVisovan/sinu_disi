﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Sinu.Models
{
    public class User
    {
        private int idUser;
        [Required]
        private string username;
        [Required]
        private string password;
        private string nume;
        private string prenume;
        private string telefon;
        private string cnp;
        private string rol;
        private string email;
        private string Imagine;

        public int IdUser { get => idUser; set => idUser = value; }
        public string Username { get => username; set => username = value; }
        public string Password { get => password; set => password = value; }
        public string Nume { get => nume; set => nume = value; }
        public string Prenume { get => prenume; set => prenume = value; }
        public string Telefon { get => telefon; set => telefon = value; }
        public string Cnp { get => cnp; set => cnp = value; }
        public string Rol { get => rol; set => rol = value; }
        public string Email { get => email; set => email = value; }
        public string Imagine1 { get => Imagine; set => Imagine = value; }
    }
}