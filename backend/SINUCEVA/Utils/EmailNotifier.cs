﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;

namespace Sinu.Utils
{
    public class EmailNotifier
    {
        public bool SendEmail(string email,string password,string recipient, string subject, string body)
        {
            try
            {
                SmtpClient smtpClient = new SmtpClient();
                smtpClient.Port = 587;
                smtpClient.Host = "smtp.gmail.com";
                smtpClient.EnableSsl = true;
                smtpClient.UseDefaultCredentials = false;
                smtpClient.Credentials = new NetworkCredential(email, password);

               
                 MailMessage emailMessage = new MailMessage();
                    emailMessage.From = new MailAddress(email);
                    emailMessage.Subject = subject;
                    emailMessage.Body = body;
                    emailMessage.To.Add(recipient);
                    emailMessage.IsBodyHtml = true;
                    smtpClient.Send(emailMessage);
                

            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }
    }
}