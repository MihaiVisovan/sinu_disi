﻿using Sinu.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Sinu.Repository
{
    public class GrupaRepo
    {
        DBConnection db = new DBConnection();

        public List<User> GetAllStudentNoGrupa()
        {
            SqlConnection con = db.GetConnection();
            try
            {
                string query = @"  select [User].idUser,[User].nume,[User].prenume from [User] where [User].idUser  not in (select [StudentGrupa].idStudent from [StudentGrupa]) and [User].rol='STUDENT'";
                SqlCommand cmd = new SqlCommand(query, con);

                SqlDataReader reader = cmd.ExecuteReader();
                List<User> studentiFaraGrupa = new List<User>();
                while (reader.Read())
                {
                    User user = new User();
                    user.IdUser = int.Parse(reader["idUser"].ToString());
                    user.Nume = reader["nume"].ToString();
                    user.Prenume = reader["prenume"].ToString();


                    studentiFaraGrupa.Add(user);
                }
                return studentiFaraGrupa;
            }
            catch (Exception e) { throw e; }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        public int getIdGrupaByName(int numarGrupa)
        {
            SqlConnection con = db.GetConnection();

            try
            {
                string query = @" select idGrupa  FROM [BDSinu].[dbo].[Grupa] where [numarGrupa]=@numarGrupa ";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.Parameters.AddWithValue("@numarGrupa", numarGrupa);
                SqlDataReader reader = cmd.ExecuteReader();

                int idGrupa = 0;
                while (reader.Read())
                {

                    idGrupa = Convert.ToInt16(reader["idGrupa"].ToString());

                }
                return idGrupa;
            }
            catch (Exception e) { throw e; }
            finally
            {
                con.Close();
                con.Dispose();
            }

        }

        public string AddGrupaBySecretar(Grupa grupa)
        {
            SqlConnection con = db.GetConnection();
            try
            {
                string query = @"INSERT into [BDSinu].[dbo].[Grupa] VALUES (@numarGrupa,@an,@nrMaxStudentiPerGrupa) ";
                SqlCommand cmd = new SqlCommand(query, con);

                cmd.Parameters.AddWithValue("@numarGrupa", grupa.numarGrupa);
                cmd.Parameters.AddWithValue("@an", grupa.an);
                cmd.Parameters.AddWithValue("@nrMaxStudentiPerGrupa", grupa.nrMaxStudentiPerGrupa);
                cmd.ExecuteNonQuery();
                return "ok";
            }
            catch (Exception e) { throw e; }
            finally
            {
                con.Close();
                con.Dispose();
            }


        }

        public string AddStudentInGrupa(int idStudent, int idGrupa)
        {
            SqlConnection con = db.GetConnection();

            try
            {
                string query = @"INSERT into [BDSinu].[dbo].[StudentGrupa] VALUES (@idStudent,@idGrupa)";
                SqlCommand cmd = new SqlCommand(query, con);

                cmd.Parameters.AddWithValue("@idStudent", idStudent);
                cmd.Parameters.AddWithValue("@idGrupa", idGrupa);
                cmd.ExecuteNonQuery();
                return "ok";
            }
            catch (Exception e) { throw e; }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        public string DeleteStudentFromGrupa(int idStudent)
        {
            SqlConnection con = db.GetConnection();

            try
            {
                string query = @"DELETE from [BDSinu].[dbo].[StudentGrupa] where idStudent = @idStudent";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.Parameters.AddWithValue("@idStudent", idStudent);
                cmd.ExecuteNonQuery();
                return "ok";
            }
            catch (Exception e) { throw e; }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        public string UpdateGrupaStudent(int idStudent, int idGrupa)
        {
            SqlConnection con = db.GetConnection();

            try
            {
                string query = @"UPDATE [BDSinu].[dbo].[StudentGrupa] SET idGrupa = @idGrupa WHERE idStudent = @idStudent";

                SqlCommand cmd = new SqlCommand(query, con);
                cmd.Parameters.AddWithValue("@idGrupa", idGrupa);
                cmd.Parameters.AddWithValue("@idStudent", idStudent);
                cmd.ExecuteNonQuery();
                return "ok";
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        public List<Grupa> GetAllGrupa()
        {
            SqlConnection con = db.GetConnection();
            try
            {
                string query = @"select * from [BDSinu].[dbo].[Grupa] ";
                SqlCommand cmd = new SqlCommand(query, con);

                SqlDataReader reader = cmd.ExecuteReader();
                List<Grupa> grupe = new List<Grupa>();
                while (reader.Read())
                {
                    Grupa grupa = new Grupa();
                    grupa.idGrupa = int.Parse(reader["idGrupa"].ToString());
                    grupa.an = reader["an"].ToString();
                    grupa.numarGrupa = reader["numarGrupa"].ToString();
                    grupa.nrMaxStudentiPerGrupa = reader["nrMaxStudentiPerGrupa"].ToString();
                    grupe.Add(grupa);
                }
                return grupe;
            } catch (Exception e) { throw e; }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }
    }
}