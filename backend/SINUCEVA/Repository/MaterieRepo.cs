﻿using Sinu.Models;
using Sinu.Repository;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace SINUCEVA.Repository
{
    public class MaterieRepo
    {
        DBConnection db = new DBConnection();
        public List<string> GetMaterie(int id)
        {

            SqlConnection con = db.GetConnection();
            try
            {
                string query = @"select denumire from Materie where idProfesor=@id";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.Parameters.AddWithValue("@id", id);
                SqlDataReader reader = cmd.ExecuteReader();

                List<string> data = new List<string>();
                while (reader.Read())
                {

                    data.Add(reader["denumire"].ToString());

                }

                return data;



            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }

        }

        public int GetIdMaterie(string denumire)
        {

            SqlConnection con = db.GetConnection();
            try
            {
                string query = @"select idMaterie from Materie where denumire=@denumire";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.Parameters.AddWithValue("@denumire", denumire);
                SqlDataReader reader = cmd.ExecuteReader();
                int IdMaterie = 0;
                //List<string> data = new List<string>();
                while (reader.Read())
                {
                    IdMaterie = Convert.ToInt16(reader["idMaterie"].ToString());
                    //data.Add(reader["denumire"].ToString());

                }

                return IdMaterie;

            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        public List<string> getAllMaterie()
        {


            SqlConnection con = db.GetConnection();
            try
            {
                string query = @"select[denumire] from[BDSinu].[dbo].[Materie]";
                SqlCommand cmd = new SqlCommand(query, con);
                SqlDataReader reader = cmd.ExecuteReader();

                List<string> data = new List<string>();
                while (reader.Read())
                {

                    data.Add(reader["denumire"].ToString());

                }

                return data;



            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }


        }
    }
}