﻿using Sinu.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Sinu.Utils;

namespace Sinu.Repository
{
    public class UserRepo
    {
        DBConnection db = new DBConnection();
        public string UpdatePersonBySecretar(User user)
        {
            SqlConnection con = db.GetConnection();
            try
            {
                if (user.IdUser != 0)
                {
                    string query = @"UPDATE [BDSinu].[dbo].[User] SET nume =@nume , prenume=@prenume,telefon=@telefon,CNP=@cnp,email=@email WHERE idUser=@idUser";

                    SqlCommand cmd = new SqlCommand(query, con);

                    cmd.Parameters.AddWithValue("@idUser", user.IdUser);
                    cmd.Parameters.AddWithValue("@email", user.Email);
                    cmd.Parameters.AddWithValue("@cnp", user.Cnp);
                    cmd.Parameters.AddWithValue("@prenume", user.Prenume);
                    cmd.Parameters.AddWithValue("@nume", user.Nume);
                    cmd.Parameters.AddWithValue("@telefon", user.Telefon);
                    cmd.ExecuteNonQuery();


                }

                return "ok";

            }
            catch (Exception e) { throw e; }
            finally
            {
                con.Close();
                con.Dispose();
            }


        }

        public List<User> GetAllUsers()
        {
            SqlConnection con = db.GetConnection();
            try
            {
                string query = @"select * from [BDSinu].[dbo].[User] ";
                SqlCommand cmd = new SqlCommand(query, con);

                SqlDataReader reader = cmd.ExecuteReader();
                List<User> users = new List<User>();
                while (reader.Read())
                {
                    User user = new User();
                    user.IdUser = int.Parse(reader["idUser"].ToString());
                    user.Nume = reader["nume"].ToString();
                    user.Prenume = reader["prenume"].ToString();
                    user.Telefon = reader["telefon"].ToString();
                    user.Cnp = reader["CNP"].ToString();
                    user.Email = reader["email"].ToString();
                    users.Add(user);
                }
                return users;
            }
            catch (Exception e) { throw e; }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        public User GetStudentById(string id)
        {
            SqlConnection con = db.GetConnection();
            try
            {
                string query = @"select * from [BDSinu].[dbo].[User] where idUser = @idUser";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.Parameters.AddWithValue("@idUser", id);
                SqlDataReader reader = cmd.ExecuteReader();
                User user = new User();

                while (reader.Read())
                {

                    user.IdUser = int.Parse(reader["idUser"].ToString());
                    user.Username = reader["username"].ToString();
                    user.Password = reader["password"].ToString();
                    user.Nume = reader["nume"].ToString();
                    user.Prenume = reader["prenume"].ToString();
                    user.Telefon = reader["telefon"].ToString();
                    user.Cnp = reader["CNP"].ToString();
                    user.Rol = reader["rol"].ToString();
                    user.Email = reader["email"].ToString();
                }

                return user;
            }
            catch (Exception e) { throw e; }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        public User GetStudentByEmail(string email)
        {
            SqlConnection con = db.GetConnection();
            try
            {
                string query = @"select * from [BDSinu].[dbo].[User] where email = @email";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.Parameters.AddWithValue("@email", email);
                SqlDataReader reader = cmd.ExecuteReader();
                User user = new User();

                while (reader.Read())
                {

                    user.IdUser = int.Parse(reader["idUser"].ToString());
                    user.Username = reader["username"].ToString();
                    user.Password = reader["password"].ToString();
                    user.Nume = reader["nume"].ToString();
                    user.Prenume = reader["prenume"].ToString();
                    user.Telefon = reader["telefon"].ToString();
                    user.Cnp = reader["CNP"].ToString();
                    user.Rol = reader["rol"].ToString();
                    user.Email = reader["email"].ToString();
                }

                return user;
            }
            catch (Exception e) { throw e; }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        public string InsertUser(User user)
        {
            string s = "ok";
            SqlConnection con = db.GetConnection();
            try
            {

                //if (user.Rol.Equals("STUDENT"))
                //{
                //    string queryStudent = @"insert into [BDSinu].[dbo].[StudentGrupa] values (@id, NULL)";
                //    SqlCommand cmdStudent = new SqlCommand(queryStudent, con);
                //    cmd.Parameters.AddWithValue("@username", user.Username);

                //}
                string query = @"insert into [BDSinu].[dbo].[User] values(@username,@password,@nume,@prenume,@telefon,@cnp,@rol,@email,@imagine) ";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.Parameters.AddWithValue("@username", user.Username);
                cmd.Parameters.AddWithValue("@password", user.Password);
                cmd.Parameters.AddWithValue("@nume", user.Nume);
                cmd.Parameters.AddWithValue("@prenume", user.Prenume);
                cmd.Parameters.AddWithValue("@telefon", user.Telefon);
                cmd.Parameters.AddWithValue("@cnp", user.Cnp);
                cmd.Parameters.AddWithValue("@rol", user.Rol);
                cmd.Parameters.AddWithValue("@email", user.Email);
                cmd.Parameters.AddWithValue("@imagine", DBNull.Value);

                SqlDataReader reader = cmd.ExecuteReader();

                //EmailNotifier email = new EmailNotifier();
                //email.SendEmail(user.Email, user.Password, user.Username, "Register", "Aveti cont pe SINU GG!");
                return s;

            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }
        public string UpdatePersonByStudent(string mail, string telefon, string id)
        {
            SqlConnection con = db.GetConnection();
            try
            {
                if (id != null)
                {
                    string query = @"UPDATE [BDSinu].[dbo].[User] SET telefon=@telefon,email=@email WHERE idUser=@idUser";

                    SqlCommand cmd = new SqlCommand(query, con);

                    cmd.Parameters.AddWithValue("@idUser", id);
                    cmd.Parameters.AddWithValue("@email", mail);

                    cmd.Parameters.AddWithValue("@telefon", telefon);
                    cmd.ExecuteNonQuery();


                }

                return "ok";

            }
            catch (Exception e) { throw e; }
            finally
            {
                con.Close();
                con.Dispose();
            }


        }
    }
}