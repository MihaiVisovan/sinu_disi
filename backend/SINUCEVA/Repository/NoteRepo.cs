﻿using Sinu.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Sinu.Repository
{
    public class NoteRepo
    {

        DBConnection db = new DBConnection();
        NotaStudent notaStudent = new NotaStudent();
        public string NotifyStudent(Note nota)
        {

            string s = "ok";
            SqlConnection con = db.GetConnection();
            try
            {
                string query = @"update [BDSinu].[dbo].[Nota] set  [BDSinu].[dbo].[Nota].[notificare]=" + 1 + " where (IdNota=@id and idMaterie=@idMaterie and idStudent=@idStudent)";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.Parameters.AddWithValue("@idStudent", nota.idStudent);
                cmd.Parameters.AddWithValue("@idMaterie", nota.idMaterie);
                cmd.Parameters.AddWithValue("@nota", nota.nota);
                cmd.Parameters.AddWithValue("@id", nota.idNota);
                SqlDataReader reader = cmd.ExecuteReader();
                return s;

            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }

        }


        public string NotifiedStudent(Note nota)
        {
            string s = "ok";
            SqlConnection con = db.GetConnection();
            try
            {
                string query = @"update [BDSinu].[dbo].[Nota] set  [BDSinu].[dbo].[Nota].[notificare]=" + 0 + " where (IdNota=@id and idMaterie=@idMaterie and idStudent=@idStudent)";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.Parameters.AddWithValue("@idStudent", nota.idStudent);
                cmd.Parameters.AddWithValue("@idMaterie", nota.idMaterie);
                cmd.Parameters.AddWithValue("@nota", nota.nota);
                cmd.Parameters.AddWithValue("@id", nota.idNota);
                SqlDataReader reader = cmd.ExecuteReader();
                return s;
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }
        public List<NotaStudent> GetNote(string materie)
        {
            SqlConnection con = db.GetConnection();
            try
            {
                string query = @"select [User].idUser, Nota.idNota, Nota.nota ,[User].nume,[User].prenume from Nota inner join Materie on Materie.idMaterie=Nota.idMaterie inner join [User] on [Nota].idStudent=[User].idUser where Materie.denumire=@materie";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.Parameters.AddWithValue("@materie", materie);
                SqlDataReader reader = cmd.ExecuteReader();
                //List<Result> resultList = new List<Result>();
                List<NotaStudent> noteStudents = new List<NotaStudent>();
                while (reader.Read())
                {
                    noteStudents = new List<NotaStudent>();
                    notaStudent.IdUser = Convert.ToInt16(reader["idUser"]);
                    notaStudent.IdNota = Convert.ToInt16(reader["idNota"]);
                    notaStudent.Nota = Convert.ToInt16(reader["nota"]);
                    notaStudent.Nume = reader["nume"].ToString();
                    notaStudent.Prenume = reader["prenume"].ToString();

                    noteStudents.Add(notaStudent);

                }
                return noteStudents;

            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }

        }

        public string UpdateNote(Note nota)
        {
            string s = "ok";
            SqlConnection con = db.GetConnection();
            try
            {
                string query = @"update [BDSinu].[dbo].[Nota] set  [BDSinu].[dbo].[Nota].[nota]=@nota where (IdNota=@id and idMaterie=@idMaterie and idStudent=@idStudent)";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.Parameters.AddWithValue("@idStudent", nota.idStudent);
                cmd.Parameters.AddWithValue("@idMaterie", nota.idMaterie);
                cmd.Parameters.AddWithValue("@nota", nota.nota);
                cmd.Parameters.AddWithValue("@id", nota.idNota);
                SqlDataReader reader = cmd.ExecuteReader();

                NotifyStudent(nota);
                return s;


            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        public string PutNote(Note nota)
        {
            string s = "ok";
            SqlConnection con = db.GetConnection();
            try
            {
                string query = @"insert into [BDSinu].[dbo].[Nota] values(@idMaterie,@idStudent,@nota) ";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.Parameters.AddWithValue("@idMaterie", nota.idMaterie);
                cmd.Parameters.AddWithValue("@idStudent", nota.idStudent);
                cmd.Parameters.AddWithValue("@nota", nota.nota);
                SqlDataReader reader = cmd.ExecuteReader();

                return s;

            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }

        }

        public List<Note> getNoteStudent(string id)
        {
            SqlConnection con = db.GetConnection();
            try
            {
                string query = @"select Materie.denumire ,nota,Materie.numarDeCredite, Nota.notificare  from materie inner join nota on Materie.idMaterie=nota.idMaterie where idStudent=@id";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.Parameters.AddWithValue("@id", id);
                SqlDataReader reader = cmd.ExecuteReader();
                //List<Result> resultList = new List<Result>();
                List<Note> noteStudents = new List<Note>();
                while (reader.Read())
                {
                    Note n = new Note();
                    n.denumire = reader["denumire"].ToString();
                    n.nota = reader["nota"].ToString();
                    n.nrCredite = reader["numarDeCredite"].ToString();
                    n.notificare = (bool)reader["notificare"];
                    noteStudents.Add(n);

                }
                return noteStudents;

            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }
    }
}