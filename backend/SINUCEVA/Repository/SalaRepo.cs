﻿using Sinu.Repository;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace SINUCEVA.Repository
{
    public class SalaRepo
    {

        DBConnection db = new DBConnection();

        public List<string> getAllSala()
        {


            SqlConnection con = db.GetConnection();
            try
            {
                string query = @"  select [denumire] from [BDSinu].[dbo].[Sala]";
                SqlCommand cmd = new SqlCommand(query, con);
                SqlDataReader reader = cmd.ExecuteReader();

                List<string> data = new List<string>();
                while (reader.Read())
                {

                    data.Add(reader["denumire"].ToString());

                }

                return data;



            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }


        }
    }
}