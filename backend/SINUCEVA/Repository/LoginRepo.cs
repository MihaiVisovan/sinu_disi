﻿using Sinu.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Sinu.Repository
{
    public class LoginRepo
    {
        public User getRol(string username, string password)
        {
            User u = new User();
            DBConnection db = new DBConnection();
            SqlConnection con = db.GetConnection();
            try
            {
                string query = @"select * from[BDSinu].[dbo].[User] where((username = @username or email = @email) and password = @password)";
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.Parameters.AddWithValue("@username", username);
                cmd.Parameters.AddWithValue("@email", username);
                cmd.Parameters.AddWithValue("@password", password);
                SqlDataReader reader = cmd.ExecuteReader();
                List<Result> resultList = new List<Result>();
                if (reader.HasRows)
                    while (reader.Read())
                    {
                        List<string> data = new List<string>();

                        u.IdUser=Convert.ToInt16(reader["idUser"].ToString());
                        u.Username=reader["username"].ToString();
                        u.Password=reader["password"].ToString();
                        u.Nume=reader["nume"].ToString();
                        u.Prenume=reader["prenume"].ToString();
                        u.Telefon=reader["telefon"].ToString();
                        u.Cnp=reader["CNP"].ToString();
                        u.Rol=reader["rol"].ToString();
                        u.Email=reader["email"].ToString();

                    }
                
                return u;



            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }
    }
}