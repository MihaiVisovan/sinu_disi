﻿using Sinu.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Sinu.Repository
{
    public class SesiuneRepo
    {
        DBConnection db = new DBConnection();

        public string InsertSesiune(Sesiune sesiune)
        {
            string ok = "ok";
            string id = "";
            SqlConnection con = db.GetConnection();
            try
            {
                string sql1 = @"  select  idMaterie from Materie where denumire=@denumire";

                SqlCommand cmd1 = new SqlCommand(sql1, con);
                cmd1.Parameters.AddWithValue("@denumire", sesiune.Denumire);
                SqlDataReader reader = cmd1.ExecuteReader();

                while (reader.Read())
                {
                    id = reader["idMaterie"].ToString();
                }
                reader.Close();
                string query = @"insert into [BDSinu].[dbo].[Sesiune] values(@IdMaterie,@dataSiOra,@sala)";
                SqlCommand cmd = new SqlCommand(query, con);

                cmd.Parameters.AddWithValue("@IdMaterie", id);
                cmd.Parameters.AddWithValue("@dataSiOra", sesiune.DataOra);
                cmd.Parameters.AddWithValue("@sala", sesiune.Sala);
                reader = cmd.ExecuteReader();



                return ok;

            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }

        }

        public List<Sesiune> GetSesiuneDenumire()
        {
            SqlConnection con = db.GetConnection();
            try
            {
                string query = @"select s.idSesiune,m.denumire,s.dataSiOra,s.sala from [BDSinu].[dbo].[Sesiune] as s,[BDSinu].[dbo].[Materie] as m where s.idMaterie=m.idMaterie";
                SqlCommand cmd = new SqlCommand(query, con);
                SqlDataReader reader = cmd.ExecuteReader();
                List<Sesiune> sessions = new List<Sesiune>();
                while (reader.Read())
                {
                    Sesiune sesiune = new Sesiune();
                    sesiune.IdSesiune = Convert.ToInt16(reader["idSesiune"].ToString());
                    sesiune.Denumire = reader["denumire"].ToString();
                    sesiune.DataOra = DateTime.Parse(reader["dataSiOra"].ToString());
                    sesiune.Sala = reader["sala"].ToString();
                    sessions.Add(sesiune);
                }
                return sessions;

            }
            catch (Exception e) { throw e; }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        public string DeleteSesiune(string id)
        {
            string ok = "ok";

            SqlConnection con = db.GetConnection();
            try
            {
                string query = @" delete Sesiune where idSesiune=@id";
                SqlCommand cmd = new SqlCommand(query, con);

                cmd.Parameters.AddWithValue("@id", id);
                SqlDataReader reader = cmd.ExecuteReader();




                return ok;

            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }

        }

        public string updateSesiune(string id, string sala)
        {
            string ok = "ok";

            SqlConnection con = db.GetConnection();
            try
            {
                string query = @"  update sesiune set sala=@sala where idSesiune=@id";
                SqlCommand cmd = new SqlCommand(query, con);

                cmd.Parameters.AddWithValue("@id", id);
                cmd.Parameters.AddWithValue("@sala", sala);
                SqlDataReader reader = cmd.ExecuteReader();




                return ok;

            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }

        }
    }
}