import React, { Component } from 'react';
import logoImage from '../Images/Sinu_Logo_Login.png';
import './Login.css';
import axios from 'axios';

class Login extends Component{

state = {
    username: "",
    password: "",
    id: 0,
    role: "",
    errorText: ''
}


logIn()
{
    return(
    <div className = "login-padding">
        <form className="login">
            <img src={logoImage} className = "logIn-sinu" />
            <input 
                placeholder = "Enter your email or username..."
                type="text" 
                className="login-input" 
                required ="required"
                onChange={(e) => this.setState({username: e.target.value})}
            />
            <input
                placeholder = "Enter your password..."
                type="password" 
                className="login-input"
                required ="required"
                onChange={e => this.setState({password: e.target.value})}
            />
            <button  className="login-button" onClick = {(e) => this.checkAccount(e)}> Log In </button>
        </form>
    </div>
    );
}

setStateFunction() {
    this.setState({
        showPopup: true
    })
}

checkPopup(){
 
}

checkAccount = (e) => {
    e.preventDefault(); 

    this.setState({
        errorText: ""
    })
    const user =
    {
        username : this.state.username,
        password : this.state.password,
        id : this.state.id,
        role: this.state.role
    } 
    if(this.state.username != "" && this.state.password != ""){

        axios.get(`http://localhost:61038/api/getRol?username=${this.state.username}&password=${this.state.password}`)
            .then(res =>
                {
                    if(res.data.Password === user.password && res.data.Username === user.username)
                    {
                        switch(res.data.Rol){
                            case "SECRETAR": {
                                            localStorage.loggedInUser = JSON.stringify(res.data)
                                            window.location = './secretariat';
                                            }   
                                            break;
                            case "PROFESOR": {
                                            localStorage.loggedInUser = JSON.stringify(res.data)
                                            window.location = './teacher';
                                            }   
                                            break;
                            case "STUDENT": {
                                            localStorage.loggedInUser = JSON.stringify(res.data)
                                            window.location = './student';
                                            break;
                                            }
                            default: return null;
                        }
                    }
                    else{
                        this.setState({
                            errorText: "Ati introdus numele sau parola gresit..."
                        })
                    }
                }
            )
        }
    else{
        this.setState({
            errorText: ""
        }, () => this.setState({
            errorText: "Campurile nu pot ramane goale!"
        }))
        
    }
}

render(){
    return(
        <div>
            {this.state.errorText && <span className="popupLogin">{this.state.errorText}</span>}    
            <div>
            {this.logIn()}
            </div>
        </div>
    );
}
}

export default Login;