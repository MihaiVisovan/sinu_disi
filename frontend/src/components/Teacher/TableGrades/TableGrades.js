import React, { Component } from 'react';
import './TableGrades.css';
import axios from 'axios';
import '../../Popup/Popup.css';

class TableGrades extends Component {

state = {
    courses: [],
    optionCourse: 'Option',
    elements: [],
    idMaterie: 0,
    errorText:''
}

componentDidMount()
{
    axios.get(`http://localhost:61038/api/getMaterieByIdProfesor?id=${JSON.parse(localStorage.loggedInUser)["IdUser"]}`)
    .then(res =>
        {
            this.setState({
                courses: res.data
            });
        }
        )
}

handleSelectChange = (event) => {
        
    this.setState({
        elements: []
    }, () => {})

    this.setState({
        optionCourse: event.target.value,
    }, () => { 
        axios.get(`http://localhost:61038/api/getNote?materie=${this.state.optionCourse}`)
        .then(res =>
            {
                this.setState({
                    elements: res.data
                });
            }
        ) });
}

handleChange = (e, elem) =>{

    let newElements = this.state.elements;
    newElements.find(x => x.IdStudent == elem.IdStudent).Nota = e.target.value;
    this.setState({
        elements: newElements
    })

    axios.get(`http://localhost:61038/api/getIdMaterieByDenumire?denumire=${this.state.optionCourse}`)
    .then(res =>
        {
            this.setState({
                idMaterie: res.data
            });
        }
    )

}

changeGrade = (elem) => {
    
    const notaEdit = {
        idNota: elem.IdNota,
        idMaterie: this.state.idMaterie,
        idStudent: elem.IdUser,
        nota: elem.Nota,
        denumire: this.state.optionCourse
    };

    this.setState({
        errorText: ''
    })

    if(notaEdit.nota != "")
    {
        if(notaEdit.nota >= 1 && notaEdit.nota <= 10){
            axios.put(`http://localhost:61038/api/updateNota`, notaEdit)
            .then(res => {} )
            localStorage.notaStudent = JSON.stringify(notaEdit)
            this.setState({
                errorText: ''
            },() => this.setState({
                errorText: "Ati editat nota cu succes!"
            }))
        }
        else{
            this.setState({
                errorText: ''
            },() => this.setState({
                errorText: "Nota trebuie sa fie intre 1 si 10!"
            }))
        }
    }
    else{
        this.setState({
            errorText: ''
        },() => this.setState({
            errorText: "Campurile nu pot ramane goale!"
        }))
    }

}

handleKeyPress = (event, elem) => {
    if(event.key == 'Enter'){
      this.changeGrade(elem)
    }
  }

showTable () {
        return  <div>
                    <table>    
                        <tbody>
                            <tr> 
                                <th> Nume </th>
                                <th> Nota </th>
                            </tr> 
                        </tbody> 
                        <tbody>{this.state.elements.map((elem) => 
                            <tr key={elem.IdUser}>
                                <td>{elem.Nume}  {elem.Prenume}</td>
                                <td>
                                    <input id = "input" placeholder = "Enter grade" value = {elem.Nota} onKeyPress = {(e) => this.handleKeyPress(e, elem)} onChange={(e) => this.handleChange(e, elem)}/>
                                    <button className = "td_button" onClick={() => this.changeGrade(elem)}> Edit </button>
                                </td>
                            </tr>
                            )}
                        </tbody>    
                    </table> 
                </div>
}

render() {
    return( 
        <div> 
            {this.state.errorText && <span className="popup">{this.state.errorText}</span>}
            <select onChange={(e) => this.handleSelectChange(e)}>
                {this.state.courses.map((x,i) => 
                    <option key={i}>{x}</option>
                )} 
            </select>
            {this.showTable()}  
        </div>
        );
    }
}

export default TableGrades;