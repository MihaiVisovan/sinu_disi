import React, { Component } from 'react';
import './ToolbarTeacher.css';
import logoImage from '../../Images/Sinu_Logo_Login.png';
import TableGrades from '../TableGrades/TableGrades';

class ToolbarTeacher extends Component {

state = {
    showComponent: false
};

handleClick() {
    this.setState({
        showComponent: true,
    });
};

logout(e)
{
    e.preventDefault();
    localStorage.loggedInUser = ""
    window.location = '/';
}

render() {
    return(
        <div className ="scroll"> 
        <button onClick = {(e) => this.logout(e)} className = "logout_button"> Logout </button> 
            <span className = "toolbarTeacher">
                <button onClick={() => this.handleClick()} className ="toolbarTeacher_button"> Note </button>             
            </span>
            <span className = "logo">
                <img id="imageTeacher" src={logoImage}></img>
            </span>
            {this.state.showComponent ?
                <TableGrades /> :
                null
            }
        </div>
    );
}
}
export default ToolbarTeacher;