import React, {Component} from 'react';
import './InfoStudent.css';
import axios from'axios';
import '../../../Popup/Popup.css';


class InfoStudent extends Component{

    state = {
        idUser: 0,
        email: "",
        cnp: "",
        prenume:"",
        nume:"",
        telefon:"",
        errorText: ""
    }

    checkEditInformation(e){

        e.preventDefault();
        const student = {
            idUser: this.state.idUser,
            username: "",
            password: "",
            nume: "",
            prenume: "",
            telefon: this.state.telefon,
            cnp: "",
            rol: "",
            email: this.state.email,
            Imagine: ""
        }
        let emailChecker = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        let testEmail = emailChecker.test((this.state.email).toLowerCase());
        if(testEmail == false){
            this.setState({
                errorText: ""
            }, () => this.setState(
            {
            errorText: "Adresa de mail este gresita!"
            }))
        }

        let phoneChecker = /^(\+4|)?(07[0-8]{1}[0-9]{1}|02[0-9]{2}|03[0-9]{2}){1}?(\s|\.|\-)?([0-9]{3}(\s|\.|\-|)){2}$/;
        let testPhone = phoneChecker.test((this.state.telefon).toLowerCase());
        if(testPhone == false)
        {
            this.setState({
                errorText: ""
            }, () => this.setState(
            {
            errorText: "Numarul de telefon este gresit!"
            }))
        }

        if(testEmail == true && testPhone == true){
            axios.put(`http://localhost:61038/api/updateStudent`, student)
                .then(res => {} )
                this.setState({
                    errorText: ""
                }, () => this.setState(
                {
                errorText: "V-ati salvat datele cu succes!"
                }))
        }
    }

    componentDidMount()
    {
        axios.get(`http://localhost:61038/api/getStudentById?id=${JSON.parse(localStorage.loggedInUser)["IdUser"]}`)
            .then(res =>
                {
                    this.setState({
                        idUser: JSON.parse(localStorage.loggedInUser)["IdUser"],
                        email: res.data.Email,
                        cnp: res.data.Cnp,
                        prenume: res.data.Prenume,
                        nume: res.data.Nume,
                        telefon: res.data.Telefon
                    });

                }
            )
    }


    render(){
        return <div>
            {this.state.errorText && <span className="popup">{this.state.errorText}</span>}
            <div className = "addInfoStudent-padding">
                <form className="addInfo">
                    <span className="addInfoStudent-Text">{this.state.nume} {this.state.prenume}</span><br/>
                    <input
                        value = {this.state.email || ''}
                        placeholder = "Enter email..."
                        type="text"
                        className=" addInfoStudent-input"
                        required
                        onChange={e => this.setState({email: e.target.value})}
                    />
                    <input
                        value = {this.state.cnp}
                        type="text"
                        className="addInfoStudent-input"
                        required
                        readOnly
                    />
                    <input
                        value = {this.state.telefon || ''}
                        placeholder = "Enter phone number..."
                        type="text"
                        className="addInfoStudent-input"
                        required
                        onChange={e => this.setState({telefon: e.target.value})}
                    />
                    <div>
                        <button  className="addInfo-button" onClick = {(e) => this.checkEditInformation(e)}> Edit </button>
                    </div>
                </form>
            </div>
        </div>
    }

}

export default InfoStudent;