import React, { Component } from 'react';
import './InfoNota.css';
import axios from 'axios';

class InfoNota extends Component {

    state = {
        elements: []
    }

    componentDidMount()
    {

        axios.get(`http://localhost:61038/api/getNoteStudent?id=${JSON.parse(localStorage.loggedInUser)["IdUser"]}`)
        .then(res =>
            {
                this.setState({
                    elements: res.data
                });
            }
        ) 
    }
    
    showTable () {
        return  <div>
            <table >
                <tbody>
                <tr>
                    <th> Materie </th>
                    <th> Nota </th>
                    <th> Credite</th>
                </tr>
                </tbody>
                <tbody>{this.state.elements.map((elem, index) => 
                        <tr key={index}>
                            <td>{elem.denumire}</td> 
                            <td> {elem.nota} </td>
                            <td> {elem.nrCredite}</td>
                        </tr>
                        )}
                </tbody>   
            </table>
        </div>
    }

    render() {
        return(
            <div>
                {this.showTable()}
            </div>
        );
    }
}

export default InfoNota;