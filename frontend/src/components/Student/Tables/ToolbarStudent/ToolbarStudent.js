import React, { Component } from 'react';
import './ToolbarStudent.css';
import logoImage from '../../../../components/Images/Sinu_Logo_Login.png'
import InfoNota from '../../Tables/TabelInfoNota/InfoNota';
import InfoStudent from '../../Tables/TabelInfoStudent/InfoStudent';
import '../../../Popup/Popup.css';
import axios from 'axios';


class ToolbarStudent extends Component {

    state = {
        tableNumber: 0,
        notifyText: "",
        elements: []
    };

    showTable () {

        switch(this.state.tableNumber) {
            case 1:
                return <InfoStudent/>;
            case 2:
                return <InfoNota/>;
            default:
                return null;
        }
    };
    makePopup(){
        
        let elements = this.state.elements;
        let checkNote = elements.find(x => x.notificare == true);
        if(checkNote != undefined){
            this.setState({
                notifyText: ""
            }, () => this.setState ({
                notifyText : "Ai primit nota " +  checkNote.nota + " la materia " + checkNote.denumire 
            }))
        }

        let notaStudent = JSON.parse(localStorage.notaStudent)
        axios.put(`http://localhost:61038/api/notifiedStudent`,notaStudent)
        .then(res =>
            {
                
            })
    }

    componentDidMount()
    {
        axios.get(`http://localhost:61038/api/getNoteStudent?id=${JSON.parse(localStorage.loggedInUser)["IdUser"]}`)
        .then(res =>
            {
                this.setState({
                    elements: res.data
                }, () => {
                  this.makePopup()
                })
            }
        )
    }

  
    logout(e)
    {
        e.preventDefault();
        localStorage.loggedInUser = "";
        
        window.location = '/';
    }

    render() {
        return(
            <div className ="scroll">
            {this.state.notifyText && <span className="popup">{this.state.notifyText}</span>}    
                <span className = "logo">
                    <img id="imageTeacher" src={logoImage}></img>
                </span>
                <button onClick = {(e) => this.logout(e)} className = "logout_button"> Logout </button>
                <span className = "toolbarSecretariat">
                    <button onClick={() => this.setState({tableNumber: 1})} className ="toolbarSecretariat_button"> Info </button>
                    <button onClick={() => this.setState({tableNumber: 2})} className ="toolbarSecretariat_button"> Note </button>
                </span>
                <span className = "logo"></span>
                {this.showTable()}
                <div className = "footer">
                    <b> www.ac.utcluj.ro </b>
                </div>
            </div>
        );
    }
}
export default ToolbarStudent;