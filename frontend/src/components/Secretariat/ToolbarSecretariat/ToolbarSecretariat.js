import React, { Component } from 'react';
import './ToolbarSecretariat.css';
import logoImage from '../../Images/Sinu_Logo_Login.png';
import TableRegister from '../../Secretariat/Tables/TableRegister/TableRegister';
import TableGroups from '../../Secretariat/Tables/TableGroups/TableGroups';
import TableStudents from '../../Secretariat/Tables/TableStudents/TableStudents';
import TableSession from '../../Secretariat/Tables/TableSession/TableSession';

class ToolbarSecretariat extends Component {

state = {
    tableNumber: 0
};

showTable () {

    switch(this.state.tableNumber) {
        case 1: 
            return <TableRegister/>;
        case 2:
            return <TableGroups/>;
        case 3:
            return <TableStudents/>;
        case 4:
            return <TableSession/>;
        default:
            return null;
    }
};

logout(e)
{
    e.preventDefault();
    localStorage.loggedInUser = ""
    window.location = '/';
}

render() {
    return(
        <div className ="scroll"> 
        <button onClick = {(e) => this.logout(e)} className = "logout_button"> Logout </button> 
            <span className = "toolbarSecretariat">
                <button onClick={() => this.setState({tableNumber: 1})} className ="toolbarSecretariat_button"> Înregistrare </button> 
                <button onClick={() => this.setState({tableNumber: 2})} className ="toolbarSecretariat_button"> Grupe </button> 
                <button onClick={() => this.setState({tableNumber: 3})} className ="toolbarSecretariat_button"> Studenți </button> 
                <button onClick={() => this.setState({tableNumber: 4})} className ="toolbarSecretariat_button"> Creare Sesiune </button> 
            </span>
            <span className = "logo">
                <img id="imageSecretariat" src={logoImage}></img>
            </span>
            {this.showTable()}
            <div className = "footer"> 
                <b> www.ac.utcluj.ro </b>
            </div>
        </div>
    );
}
}
export default ToolbarSecretariat;