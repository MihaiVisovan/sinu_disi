import React, { Component } from 'react';
import './AddToGroup.css';
import axios from 'axios';
import logo from '../../../../Images/Sinu_Logo_Login.png'
import '../../../../Popup/Popup.css';
class AddToGroup extends Component {

state = {
    elements: [],
    groups: [],
    groupEdit: 0,
    idGroup: 0,
    errorText: ""

}

handleChange(e, elem){
    e.preventDefault();

    this.setState({
        groupEdit: e.target.value
    })

}

componentDidMount()
{
    axios.get(`http://localhost:61038/api/getStudentNoGrupa`)
    .then(res =>
        {
            this.setState({
                elements: res.data
            });
        }
    )
    axios.get(`http://localhost:61038/api/getAllGroups`)
    .then(res =>
        {
            this.setState({
                groups: res.data
            });
        }
    )
}

checkGroup()
{
    let groups = this.state.groups;
    let rezFind = groups.find(x => x.numarGrupa == this.state.groupEdit);
    if(rezFind != undefined){
        return true;
    }
    return false;
}



addGroup = (elem) => {

    let checkGroup = this.checkGroup();
  
    
    if(this.state.groupEdit >= 1000 && this.state.groupEdit<= 9999){
        if(checkGroup == false){
            this.setState({
                errorText: ""
            }, () => this.setState({
                errorText: "Grupa nu se afla in baza de date!"
            }))
        }
        else{
            axios.get(`http://localhost:61038/api/getIdGrupaByName?numarGrupa=${this.state.groupEdit}`)
            .then(res =>
                {
                    this.setState({
                        idGroup: res.data
                    }, () => {
                        axios.post(`http://localhost:61038/api/addStudentToGrupa?idStudent=${elem.IdUser}&idGrupa=${this.state.idGroup}`)
                    .then(res =>{})
                } ) 
                }
            )
    
            this.setState({
                errorText: ""
            }, () => this.setState({
                errorText: "Ati adaugat studentul la o grupa cu succes!"
            }))
        }
        axios.get(`http://localhost:61038/api/getStudentNoGrupa`)
        .then(res =>
            {
                this.setState({
                    elements: res.data
                });
            }
        )
    }
    else{
        this.setState({
            errorText: ""
        }, () => this.setState({
            errorText: "Grupa trebuie sa fie un numar de 4 cifre!"
        }))
    }
}

back(e)
{
    e.preventDefault();
    window.location = '/secretariat';
}

showTable () {
        return  <div>
                    <span className = "logo">
                        <img id="imageSecretariat" src={logo}></img>
                    </span>
                    <table>    
                        <tbody>
                            <tr> 
                                <th> Nume </th>
                                <th> Grupa </th>
                            </tr> 
                        </tbody> 
                        <tbody>{this.state.elements.map((elem) => 
                            <tr key={elem.IdUser}>
                                <td>{elem.Nume}  {elem.Prenume}</td>
                                <td>
                                    <input id = "input" placeholder = "Enter group..." value = {elem.Grupa} onChange={(e) => this.handleChange(e, elem)}/>
                                    <button className = "td_button" onClick={() => this.addGroup(elem)}> Edit </button>
                                </td>
                            </tr>
                            )}
                        </tbody>    
                    </table> 
                    <button onClick = {(e) => this.back(e)} className = "back_button"> Back </button> 
                </div>
}

render() {
    return( 
        <div> 
            {this.state.errorText && <span className="popup">{this.state.errorText}</span>}
            {this.showTable()}  
        </div>
        );
    }
}

export default AddToGroup;