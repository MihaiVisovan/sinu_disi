import React, {Component} from 'react';
import './TableStudents.css';

class TableStudents extends Component{

editInfo(e){
    e.preventDefault();
    window.location = './edit';
}

addToGroup = (e) =>{
    e.preventDefault();
    window.location = './add';
}


render (){
    return <div className="max-width">
        <button onClick = {(e) => this.editInfo(e)} className = "buttons"> Edit Info </button>
        <button onClick = {(e) => this.addToGroup(e)} className = "buttons"> Add to Group </button>
    </div>
}
}

export default TableStudents;