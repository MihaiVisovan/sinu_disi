import React, {Component} from 'react';
import './EditInformation.css';
import logoImage from '../../../../Images/Sinu_Logo_Login.png'
import axios from'axios';
import '../../../../Popup/Popup.css';

class EditInformation extends Component{

state = {
    idUser: 0,
    email: "",
    cnp: "",
    prenume:"",
    nume:"",
    telefon:"",
    elements:[],
    emailStudent: ""
}


componentDidMount(){

    axios.get(`http://localhost:61038/api/getAllUsers`)
    .then(res =>
        {
            this.setState({
                elements: res.data
            });
        }
    )
}

checkInformation = () => 
{

    if( this.state.nume == "" || this.state.prenume == "" || this.state.telefon == "" || this.state.cnp == "" || this.state.email == "")
    {
        this.setState({
            errorText: ""
        }, () => this.setState
        ({
            errorText:"Trebuie sa umpleti toate campurile!"
        }))
        return false;
    }
    else
    {

        let emailChecker = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        let testEmail = emailChecker.test((this.state.email).toLowerCase());
        if(testEmail == false)
        {
            this.setState({
                errorText: ""
            }, () => this.setState
            ({
                errorText:"Adresa de mail este gresita!"
            }))
            return false;
        }

        let cnpChecker = /^(\d{13})?$/;
        let testCnp = cnpChecker.test(this.state.cnp);
        if(testCnp == false)
        {
            this.setState({
                errorText: ""
            }, () => this.setState
            ({
                errorText:"Cnp-ul trebuie sa continta 13 cifre"
            }))
            return false;
        }

        let namesChecker = /^[a-z ,.'-]+$/i;
        let testNume = namesChecker.test((this.state.nume).toLowerCase());
        let testPrenume = namesChecker.test((this.state.prenume).toLowerCase());
        if(testNume == false || testPrenume == false)
        {
            this.setState({
                errorText: ""
            }, () => this.setState
            ({
                errorText:"Numele sau prenumele sunt gresite!"
            }))
            return false;
        }

        let phoneChecker = /^(\+4|)?(07[0-8]{1}[0-9]{1}|02[0-9]{2}|03[0-9]{2}){1}?(\s|\.|\-)?([0-9]{3}(\s|\.|\-|)){2}$/;
        let testPhone = phoneChecker.test((this.state.telefon).toLowerCase());
        if(testPhone == false)
        {
            this.setState({
                errorText: ""
            }, () => this.setState
            ({
                errorText:"Numarul de telefon este gresit!"
            }))
            return false;
        }
      
        return true;
    }
   
}


checkEditInformation(e){

    e.preventDefault();

    const user = {
        idUser: this.state.idUser,
        email: this.state.email,
        cnp: this.state.cnp,
        prenume: this.state.prenume,
        nume: this.state.nume,
        telefon: this.state.telefon
    };

    let checkInfo = this.checkInformation();

    if(checkInfo == true)
    {
        axios.put(`http://localhost:61038/api/updateStudentBySecretar`, user)
        .then(res =>
            {
                axios.get(`http://localhost:61038/api/getAllUsers`)
                .then(res =>
                    {
                        this.setState({
                            elements: res.data
                        });
                    }
                )
            }
        )
        this.setState({
            errorText: ""
        }, () => this.setState
        ({
            errorText:"Ati editat informatii cu succes!"
        }))
    }
}

searchStudent(e)
{
    e.preventDefault();
    if(this.state.emailStudent != ""){
        axios.get(`http://localhost:61038/api/getStudentByEmail?email=${this.state.emailStudent}`)
        .then(res =>
            {
                this.setState({
                    idUser: res.data.IdUser,
                    email: res.data.Email,
                    cnp: res.data.Cnp,
                    prenume: res.data.Prenume,
                    nume: res.data.Nume,
                    telefon: res.data.Telefon
                });
                
            }
        )
    }
}

keyPress = e =>
{
    e.preventDefault();
  
    if(e.keyCode == 13){
        this.checkEditInformation(e)
    }
} 

back(e)
{
    e.preventDefault();
    window.location = '/secretariat';
}



render(){
    return <div>  
            {this.state.errorText && <span className="popup">{this.state.errorText}</span>}
                <span className = "logo">
                    <img id="imageTeacher" src={logoImage}></img>
                </span>
                <form className="example">
                    <input 
                    type="text"
                    placeholder="Search.."
                    required
                    onChange = {e => this.setState({emailStudent: e.target.value})}/>
                    <button type="submit" onClick = {(e) => this.searchStudent(e)}> Search </button>
                </form>
                <div className = "addInfo-padding">
                    <form className="addInfo">
                        <input 
                            value = {this.state.email || ''}
                            placeholder = "Enter email..."
                            type="text" 
                            className="addInfo-input" 
                            required
                            onChange={e => this.setState({email: e.target.value})}
                        />
                        <input
                            value = {this.state.cnp || ''}
                            placeholder = "Enter CNP..."
                            type="text" 
                            className="addInfo-input"
                            required 
                            onChange={e => this.setState({cnp: e.target.value})}
                        />
                        <input
                            value = {this.state.prenume || ''}
                            placeholder = "Enter first name..."
                            type="text" 
                            className="addInfo-input"
                            required 
                            onChange={e => this.setState({prenume: e.target.value})}
                        />
                        <input
                            value = {this.state.nume || ''}
                            placeholder = "Enter last name..."
                            type="text" 
                            className="addInfo-input"
                            required 
                            onChange={e => this.setState({nume: e.target.value})}
                        />
                        <input
                            value = {this.state.telefon || ''}
                            placeholder = "Enter phone number..."
                            type="text" 
                            className="addInfo-input"
                            required 
                            onChange={e => this.setState({telefon: e.target.value})}
                        />
                        <div>
                            <button  className="addInfo-button" onKeyDown = {(e) => this.keyPress(e)} 
                            onClick = {(e) => this.checkEditInformation(e)}> Edit </button>
                        </div>
                    </form>
                 </div>
                 <button onClick = {(e) => this.back(e)} className = "back_button"> Back </button> 
           </div>
}

}

export default EditInformation;