import React, { Component } from 'react';
import './TableGroups.css';
import axios from 'axios';
import '../../../Popup/Popup.css';

class TableGroups extends Component {

state = {
    elements: [],
    numarGrupa: 0,
    an: 0,
    nrMaxStudentiPerGrupa: 0,
    errorText: ""
}

componentDidMount()
{
    axios.get(`http://localhost:61038/api/getAllGroups`)
    .then(res =>
        {
            this.setState({
                elements: res.data
            });
        }
    )
}

checkInformation = () => 
{

    if(this.state.numarGrupa == "" || this.state.an == "" || this.state.nrMaxStudentiPerGrupa == "")
    {
        this.setState({
            errorText:""
        }, () => this.setState({
            errorText: "Trebuie sa umpleti toate campurile!"
        }))
        return false;
    }
    else
    {
   
        if(this.state.numarGrupa < 1000 || this.state.numarGrupa > 9999)
        {
            this.setState({
                errorText:""
            }, () => this.setState({
                errorText: "Numarul grupei trebuie sa fie de 4 cifre!"
            }))
            return false;
        }

        if(this.state.nrMaxStudentiPerGrupa < 10 || this.state.nrMaxStudentiPerGrupa > 100)
        {
            this.setState({
                errorText:""
            }, () => this.setState({
                errorText: "Numarul maxim de studenti trebuie sa fie intre 10-100!"
            }))
            return false;
        }

        if(this.state.an < 0 || this.state.an > 4)
        {
            this.setState({
                errorText:""
            }, () => this.setState({
                errorText: "Anul trebuie sa fie un numar intre 1-4"
            }))
            return false;
        }
       
        return true;
    }
   
}

checkAddedGroup = (e) => {
    e.preventDefault();

    let newGroup = {
        numarGrupa: this.state.numarGrupa,
        an: this.state.an,
        nrMaxStudentiPerGrupa: this.state.nrMaxStudentiPerGrupa
    }
    
    let checkInfo = this.checkInformation();
    if(checkInfo == true)
    {

        let newGroups = this.state.elements;
        newGroups.push(newGroup);

        this.setState({
        elements: newGroups  
        },() => {
            axios.post(`http://localhost:61038/api/addGrupaBySecretar`, newGroup)
        .then(res =>
            {
                axios.get(`http://localhost:61038/api/getAllGroups`)
                .then(res =>
                    {
                        this.setState({
                            elements: res.data
                        });
                    }
                )
            }
        )
        })
        this.setState({
            errorText:""
        }, () => this.setState({
            errorText: "Ati introdus o noua grupa cu succes"
        }))
    }
    
}

showAddForm()
{
    return <div className = "add-padding">
                <form className="add">
                    <input 
                        placeholder = "Enter group number..."
                        type="text" 
                        className="add-input" 
                        required
                        onChange={e => this.setState({numarGrupa: e.target.value})}
                    />
                    <input
                        placeholder = "Enter year..."
                        type="text" 
                        className="add-input"
                        required 
                        onChange={e => this.setState({an: e.target.value})}
                    />
                     <input
                        placeholder = "Enter the maximum number of students..."
                        type="text" 
                        className="add-input"
                        required 
                        onChange={e => this.setState({nrMaxStudentiPerGrupa: e.target.value})}
                    />
                    <button  className="add-button" onClick = {(e) => this.checkAddedGroup(e)}> Add Group </button>
                </form>
           </div>
}


showTable () {
        return  <div>  
                    <table>    
                        <tbody>
                            <tr> 
                                <th> Numar Grupa </th>
                                <th> An </th>
                                <th> Numar Maxim Studenti </th>
                            </tr> 
                        </tbody> 
                        <tbody>{this.state.elements.map((elem) => 
                            <tr key={elem.idGrupa}>
                                <td>{elem.numarGrupa}</td>
                                <td>{elem.an}</td>
                                <td>{elem.nrMaxStudentiPerGrupa}</td>
                            </tr>
                            )}
                        </tbody>    
                    </table> 
                   
                </div>
}

render() {
    return( 
        <div className = "scrolling"> 
            {this.state.errorText && <span className="popup">{this.state.errorText}</span>}
            {this.showAddForm()}
            {this.showTable()}  
        </div>
        );
    }
}

export default TableGroups;