import React, {Component} from 'react';
import './TableRegister.css'
import axios from 'axios';
import '../../../Popup/Popup.css';

class TableRegister extends Component{

state = {
    roles: ["STUDENT", "PROFESOR"],
    optionRole: 'STUDENT',
    username: '',
    password: '',
    nume: '', 
    prenume: '',
    telefon: '', 
    CNP: '',
    email: '',
    errorText:''
}

handleSelectChange = (event) => {
    this.setState({
        optionRole: event.target.value
    });
}


checkInformation = () => 
{

    if(this.state.username == "" || this.state.password == "" || this.state.nume == "" ||
     this.state.prenume == "" || this.state.telefon == "" || this.state.cnp == "" || this.state.email == "")
    {
        this.setState({
            errorText:""
        }, () => this.setState({
            errorText: "Trebuie sa umpleti toate campurile!"
        }))
        return false;
    }
    else
    {
        let namesChecker = /^[a-z ,.'-]+$/i;
        let testNume = namesChecker.test((this.state.nume).toLowerCase());
        let testPrenume = namesChecker.test((this.state.prenume).toLowerCase());
        if(testNume == false || testPrenume == false)
        {
            this.setState({
                errorText:""
            }, () => this.setState({
                errorText: "Numele sau prenumele sunt gresite!"
            }))
            return false;
        }

        let phoneChecker = /^(\+4|)?(07[0-8]{1}[0-9]{1}|02[0-9]{2}|03[0-9]{2}){1}?(\s|\.|\-)?([0-9]{3}(\s|\.|\-|)){2}$/;
        let testPhone = phoneChecker.test((this.state.telefon).toLowerCase());
        if(testPhone == false)
        {
            this.setState({
                errorText:""
            }, () => this.setState({
                errorText: "Numarul de telefon este gresit!"
            }))
            return false;
        }

        let cnpChecker = /^(\d{13})?$/;
        let testCnp = cnpChecker.test(this.state.CNP);
        if(testCnp == false)
        {
            this.setState({
                errorText:""
            }, () => this.setState({
                errorText: "Cnp-ul trebuie sa continta 13 cifre"
            }))
            return false;
        }

        let emailChecker = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        let testEmail = emailChecker.test((this.state.email).toLowerCase());
        if(testEmail == false)
        {
            this.setState({
                errorText:""
            }, () => this.setState({
                errorText: "Adresa de mail este gresita!"
            }))
            return false;
        }
        return true;
    }
   
}


checkAddedGroup(e) {
    e.preventDefault();
    const user = {
        username: this.state.username,
        password: this.state.password,
        nume: this.state.nume, 
        prenume: this.state.prenume,
        telefon: this.state.telefon, 
        CNP: this.state.CNP,
        rol: this.state.optionRole,
        email: this.state.email
    }

    let checkInfo = this.checkInformation();

    if(checkInfo){
        axios.post(`http://localhost:61038/api/insertUser`, user)
            .then(res => {} )
            this.setState({
                errorText:""
            }, () => this.setState({
                errorText: "Ati introdus un nou utilizator cu succes!"
            }))
    }
}

render()
{
    return (   
            <div className = "addRegister-padding">
            {this.state.errorText && <span className="popup">{this.state.errorText}</span>}
                <form className="addRegister">
                    <input 
                        placeholder = "Introduceti username-ul..."
                        type="text" 
                        className="addRegister-input" 
                        required
                        onChange={e => this.setState({username: e.target.value})}
                    />
                    <input
                        placeholder = "Introduceti parola..."
                        type="password" 
                        className="addRegister-input2"
                        required 
                        onChange={e => this.setState({password: e.target.value})}
                    />
                     <input
                        placeholder = "Introduceti numele..."
                        type="text" 
                        className="addRegister-input"
                        required 
                        onChange={e => this.setState({nume: e.target.value})}
                    />
                    <input
                        placeholder = "Introduceti prenumele..."
                        type="text" 
                        className="addRegister-input"
                        required 
                        onChange={e => this.setState({prenume: e.target.value})}
                    />
                     <input
                        placeholder = "Introduceti numarul de telefon..."
                        type="text" 
                        className="addRegister-input"
                        required 
                        onChange={e => this.setState({telefon: e.target.value})}
                    />
                    <input
                        placeholder = "Introduceti CNP-ul..."
                        type="text" 
                        className="addRegister-input"
                        required 
                        onChange={e => this.setState({CNP: e.target.value})}
                    />
                     <input
                        placeholder = "Introduceti email-ul..."
                        type="text" 
                        className="addRegister-input"
                        required 
                        onChange={e => this.setState({email: e.target.value})}
                    />
                    <select className = "selectRegister" onClick={(e) => this.handleSelectChange(e)}>
                    {this.state.roles.map((x,i) => 
                        <option key={i}>{x}</option>
                    )} 
                     </select>
                     <br/>
                     <div className = "divAddRegister">
                    <button  className="addRegister-button" id="btnAdd" onkeydown = "if (event.keyCode == 13) document.getElementById('btnAdd').click()" 
                        onClick = {(e) => this.checkAddedGroup(e)}> Inregistrare </button>
                    </div>
                </form>
           </div>
    );
}
}

export default TableRegister;