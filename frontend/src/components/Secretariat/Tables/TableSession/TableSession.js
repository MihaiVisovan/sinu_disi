
import React, { Component } from 'react';
import './TableSession.css';
import Calendar from 'react-calendar'
import axios from 'axios';
import '../../../Popup/Popup.css';
import { timingSafeEqual } from 'crypto';

class TableSession extends Component {

state = {
    courses: [],
    classrooms: [],
    hours: ['8:00', '11:00', '14:00', '17:00'],
    optionCourse: 'Analiza Matematica',
    optionClassroom: '40',
    optionCalendar: false,
    optionHour: '8:00',
    date: new Date(),
    exams: [],
    dateTime: "",
    exams: [],
    today: new Date(),
    errorText: ""
    
}

componentDidMount()
{
    axios.get(`http://localhost:61038/api/getAllMaterie`)
            .then(res => {
                this.setState({courses: res.data})
            })

    axios.get(`http://localhost:61038/api/getAllSala`)
    .then(res => {
        this.setState({classrooms: res.data})
    })

    axios.get(`http://localhost:61038/api/getSesiune`)
    .then(res => {
        this.setState({exams: res.data})
    })
    
}

handleAddExam = (event) => {

    const date = this.state.date;
    const today = this.state.today;
    today.setHours(0);
    today.setMinutes(0);
    today.setSeconds(0);

    if(date < today)
    {
        this.setState({
            errorText:""
        }, () => this.setState({
            errorText: "Data introdusa nu este corecta!"
        }))
    }
    else{

    let optionHour = this.state.optionHour;
    date.setHours(parseInt(optionHour.split(":")[0]) + 3);

    const exam = {
        denumire: this.state.optionCourse,
        dataOra: date,
        sala: this.state.optionClassroom
    }
    console.log(exam.dataOra)

    axios.post(`http://localhost:61038/api/insertSesiune`, exam)
        .then(res => {
            this.setState({
                ...this.state,
            });
            axios.get(`http://localhost:61038/api/getSesiune`)
            .then(res => {
                this.setState({exams: res.data})
            })
        })
    }
}

handleSelectChangeCourse = (event) => {

    this.setState({
        optionCourse: event.target.value,
    });
}

handleSelectChangeClassroom = (event) => {

    this.setState({
        optionClassroom: event.target.value,
    });    
}

handleSelectChangeHours = (event) => {

    this.setState({
        optionHour: event.target.value,
    });
}

handleCalendar = (event) => {
    this.setState({ optionCalendar: true });
}

renderCalendar() {
return this.state.optionCalendar ? (
    <div >
        <Calendar
            onChange={this.onChange}
            value={this.state.date}
        />
    </div>
) : (
    <div>
    </div>
);
}

onChange = date => {
    this.setState({ date: date });
}

handleEditClassroom = (e, elem) => {
    
    let newExams = this.state.exams;
    newExams.find(x => x.IdSesiune == elem.IdSesiune).Sala = e.target.value;
    this.setState({
        exams: newExams
    })
}

changeClassroom = elem => {

    let exams = this.state.exams;
    let newSala = exams.find(x => x.IdSesiune == elem.IdSesiune).Sala;

    const sesiuneChanged = {
        idSesiune: elem.IdSesiune,
        idMaterie: "",
        dataOra: "",
        sala: newSala,
        denumire: ""
    }
    
    let checkSala = /^[a-zA-Z0-9]*$/;
    let testSala = checkSala.test(newSala);

    if(newSala == "")
    {
        this.setState({
            errorText:""
        }, () => this.setState({
            errorText: "Campul nu poate ramane gol!"
        }))
    }
    else{
        if(testSala == true)
        {
            axios.put(`http://localhost:61038/api/updateSesiune`, sesiuneChanged)
            .then(res => {})
            this.setState({
                errorText:""
            }, () => this.setState({
                errorText: "Ati modificat sala cu succes!"
            }))
        }
        else
        {
            this.setState({
                errorText:""
            }, () => this.setState({
                errorText: "Sala poate contine doar litere si numere!"
            }))
        }
    }
}


render() {
    return( 
        <div>
            {this.state.errorText && <span className="popup">{this.state.errorText}</span>}
            <div className="div_width"> 
            <div>
                <div className = "div_small_width">
                <select className = "option_font" onClick={(e) => this.handleSelectChangeCourse(e)}>
                    {this.state.courses.map((x,i) => 
                        <option key={i}>{x}</option>
                    )} 
                </select>
                </div>
                <div className = "div_small_width">
                <select className = "option_font" onClick={(e) => this.handleSelectChangeClassroom(e)}>
                    {this.state.classrooms.map((x,i) => 
                        <option key={i}>{x}</option>
                    )} 
                </select>
                </div>
                <div className = "div_small_width">
                <select className = "option_font" onClick={(e) => this.handleSelectChangeHours(e)}>
                    {this.state.hours.map((x,i) => 
                        <option key={i}>{x}</option>
                    )} 
                </select>
                </div>
            </div>
           
            <div className = "div_bigger_width">
                <button onClick = {(e) => this.handleCalendar(e)} className = "button_Calendar"> Calendar </button>
                <div className = "calendar_css">
                 {this.renderCalendar()}
                </div>
            </div>

            </div>
            <button onClick = {(e) => this.handleAddExam(e)} className = "button_AddExamen"> Adauga Examen </button>
            <div>    
            <table >    
                <tbody>
                    <tr> 
                        <th> Materie </th>
                        <th> Sala </th>
                        <th> Data Ora </th>                       
                    </tr> 
                </tbody>
                <tbody>{this.state.exams.map((elem) => 
                    <tr key={elem.IdSesiune}>
                        <td>{elem.Denumire}</td>
                        <td>
                            <input id = "input" placeholder = "Modifica sala" value = {elem.Sala} onChange={(e) => this.handleEditClassroom(e, elem)} />
                            <button className="td_button" onClick={() => this.changeClassroom(elem)} > Edit </button>
                        </td>
                        <td>{elem.DataOra}</td>
                    </tr>
                    )}
                </tbody>    
            </table> 
            </div>
        </div>    
        );
    }
}

export default TableSession;