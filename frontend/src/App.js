import React, { Component } from 'react';
import './App.css';
import { Route, Link, BrowserRouter as Router } from 'react-router-dom'
import ToolbarTeacher from './components/Teacher/ToolbarTeacher/ToolbarTeacher';
import ToolbarSecretariat from './components/Secretariat/ToolbarSecretariat/ToolbarSecretariat';
import EditInformation from './components/Secretariat/Tables/TableStudents/EditInformation/EditInformation';
import ToolbarStudent from './components/Student/Tables/ToolbarStudent/ToolbarStudent';
import Login from './components/Login/Login'
import AddToGroup from './components/Secretariat/Tables/TableStudents/AddToGroup/AddToGroup';

class App extends Component {

  render() {
    return (
      <div className="App">
        <Router>
          <div>
            <Route exact path ="/" component = {Login}/>
            <Route path="/teacher" component={ToolbarTeacher} />
            <Route path="/secretariat" component={ToolbarSecretariat} />
            <Route path="/student" component={ToolbarStudent} />
            <Route path="/edit" component={EditInformation} />
            <Route path="/add" component={AddToGroup} />
          </div>
        </Router>
      </div>
    );
  }
}

export default App;
